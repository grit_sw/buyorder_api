import arrow

from api.models import BuyOrder, BillingMethod, TimeSpanMeasure
from logger import logger


class OrderManager(object):
	"""docstring for OrderManager"""

	def create_order(self, payload, admin_id=None):
		"""
			Method to create a buy order
			@args: payload
			@returns: status code
		"""
		response = {}
		facility_id = payload['facility_id']
		user_group_id = payload['user_group_id']
		modified_payload = payload
		start_date_time = arrow.get(payload['start_date_time']).replace(tzinfo="Africa/Lagos")
		end_date_time = arrow.get(payload['end_date_time']).replace(tzinfo="Africa/Lagos")
		modified_payload['start_date_time'] = str(start_date_time)
		modified_payload['end_date_time'] = str(end_date_time)

		billing_method = BillingMethod.find_billing_method(id_=payload['billing_method_id'])
		if not billing_method:
			message = "Billing method not found"
			response['success'] = False
			response['message'] = message
			return response, 404

		if payload['time_span_measure_id']:
			time_span_measure = TimeSpanMeasure.find_time_span_measure(id_=payload['time_span_measure_id'])

			if not time_span_measure:
				message = "Time span measure not found"
				response['success'] = False
				response['message'] = message
				return response, 404

		clashing_times = BuyOrder.clashing_times(user_group_id, facility_id, start_date_time, end_date_time)
		clash_details = [order.to_dict() for order in clashing_times]
		message = 'An order with a conflicting time was found. You might want to review this order.'
		if len(clashing_times) > 1:
			logger.info("\n\n\n\n")
			logger.info("CLASHING TIMES = {}".format(clash_details))
			logger.info("\n\n\n\n")
			message = 'Conflicting order times were found. You might want to review these orders.'

		if clashing_times:
			response['success'] = False
			response['message'] = message
			response['data'] = clash_details
			return response, 409

		orders = BuyOrder.all_my_orders(user_group_id=user_group_id).all()
		duplicate = False
		dupe_order = None
		for order in orders:
			if order.payload_format() == modified_payload:
				duplicate = True
				dupe_order = order
				break

		if duplicate:
			response['success'] = False
			response['message'] = 'Order already exists.'
			response['data'] = dupe_order.to_dict()
			return response, 409

		if admin_id:
			payload['created_by'] = admin_id
		else:
			payload['created_by'] = payload['user_group_id']

		data, success = BuyOrder.create_order(**payload)
		if not success:
			# print('data = ', data)
			response['success'] = success
			response['message'] = str(data)
			return response, 400

		response['success'] = success
		response['data'] = data
		return response, 201

	def all_orders_in_facility(self, facility_id, page_no, size):
		"""
			An admin method to view all the orders in a market
		"""
		response = {}
		orders = BuyOrder.facility_orders(facility_id, page_no, size)
		logger.info(dir(orders))
		all_orders = [order.to_dict() for order in orders.items]

		if not all_orders:
			response['success'] = False
			response['message'] = 'No buy orders found.'
			return response, 404

		response['success'] = True
		response['data'] = all_orders
		response['count'] = len(all_orders)
		response['has_prev'] = orders.has_prev
		response['has_next'] = orders.has_next
		return response, 200

	def all_subnet_orders(self, subnet_id, page_no, size):
		"""
			All the orders for a subnet
		"""
		response = {}
		orders = BuyOrder.subnet_orders(subnet_id, page_no, size)
		all_orders = [order.to_dict() for order in orders.items]

		if not all_orders:
			response['success'] = False
			response['message'] = 'No buy orders found.'
			return response, 404

		response['success'] = True
		response['data'] = all_orders
		response['count'] = len(all_orders)
		response['has_prev'] = orders.has_prev
		response['has_next'] = orders.has_next
		return response, 200

	def one_order(self, order_id):
		response = {}
		order = BuyOrder.one_order(order_id)

		if not order:
			response['success'] = False
			response['message'] = 'Buy order not found.'
			return response, 404

		response['success'] = True
		response['data'] = order
		return response, 200

	def my_orders(self, user_group_id, page, size):
		response = {}
		orders = BuyOrder.my_orders(user_group_id, page, size)
		all_orders = [order.to_dict() for order in orders.items]

		if not all_orders:
			response['success'] = False
			response['message'] = 'No buy orders found.'
			return response, 404
		response['success'] = True
		response['data'] = all_orders
		response['count'] = len(all_orders)
		response['has_prev'] = orders.has_prev
		response['has_next'] = orders.has_next
		return response, 200

	def edit_order(self, payload):
		response = {}
		# print(payload)
		# print(type(payload['auto_renew']))
		order_id = payload['order_id']
		order = BuyOrder.get_order(order_id)
		if order is None:
			response['success'] = False
			response['message'] = 'Buy order not found.'
			return response, 404

		if payload['time_span_measure_id']:
			time_span_measure = TimeSpanMeasure.find_time_span_measure(id_=payload['time_span_measure_id'])

			if not time_span_measure:
				message = "Time span measure not found"
				response['success'] = False
				response['message'] = message
				return response, 404

		data = order.edit_order(payload)
		response['success'] = True
		response['data'] = data
		return response, 200

	def find_order_match(self, payload):
		"""Get all the buy orders that match a given sell order"""
		response = {}

		del payload['auto_renew']
		del payload['subnet_id']
		del payload['user_group_id']
		matches = BuyOrder.find_order_match(**payload)
		if matches:
			response['success'] = True
			response['data'] = [match.to_dict() for match in matches]
			return response, 200

		response['success'] = False
		response['message'] = 'No match found.'
		return response, 404

	def delete_order(self, order_id):
		# Ensure only a customer or a client-admin can execute this
		response = {}
		order = BuyOrder.get_order(order_id=order_id)
		if order is None:
			response['success'] = False
			response['message'] = 'Buy order not found.'
			return response, 404

		BuyOrder.delete_order(order)
		response['success'] = True
		response['message'] = 'Buy Order deleted.'
		return '', 204

	def delete_user_group(self, user_group_id):
		# Ensure only a customer or a client-admin can execute this
		response = {}
		order_obj = BuyOrder.get_user_order(user_group_id=user_group_id)
		if order_obj is None:
			response['success'] = False
			response['message'] = 'Buy order not found.'
			return response, 404
		try:
			BuyOrder.delete_all_orders(order_obj)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Buy order not found.'
			return response, 500

		response['success'] = True
		response['message'] = 'Buy Order deleted.'
		return '', 204

	def delete_facility(self, facility_id):
		# Ensure only a customer or a client-admin can execute this
		response = {}
		order_obj = BuyOrder.get_facility_order(facility_id=facility_id)
		if order_obj is None:
			response['success'] = False
			response['message'] = 'Buy order not found.'
			return response, 404
		try:
			BuyOrder.delete_all_orders(order_obj)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Buy order not found.'
			return response, 500

		response['success'] = True
		response['message'] = 'Buy Order deleted.'
		return '', 204
