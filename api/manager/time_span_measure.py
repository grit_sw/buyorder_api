from api.models import TimeSpanMeasure
from logger import logger


class TimeSpanManager(object):
	def create(self, name, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403

		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] = "Unauthorized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		duplicate_time_span_measure = TimeSpanMeasure.find_time_span_measure(name=name.title())
		if duplicate_time_span_measure:
			response['success'] = False
			response['message'] = "Time span measure {} already exists.".format(name.title())
			return response, 409

		new_time_span_measure = TimeSpanMeasure.create(time_span_measure_name=name)
		if new_time_span_measure:
			response['success'] = True
			response['data'] = new_time_span_measure
			return response, 201

		response['success'] = False
		response['message'] = 'Error encountered in creating time span measu {}'.format(name.title())
		return response, 400

	def edit(self, name, role_id, time_span_measure_id):

		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403

		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] = "Unauthorized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Invalid request"
			return response, 400

		duplicate_time_span_measure = TimeSpanMeasure.find_time_span_measure(name=name)
		if duplicate_time_span_measure:
			response['success'] = False
			response['message'] = "Time span measure with name {} already exists".format(name.title())
			return response, 409

		time_span_measure = TimeSpanMeasure.find_time_span_measure(id_=time_span_measure_id)

		if time_span_measure:
			data = time_span_measure.edit(name=name)
			response['success'] = True
			response['data'] = data
			return response, 201
		else:
			response['success'] = False
			response['message'] = "Time span measure not found"
			return response, 404


		response['success'] = False
		response['message'] = "Error editing time span measu"
		return response, 500

	def view_one(self, role_id, time_span_measure_id):

		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403

		try:
			if int(role_id) < 3:
				response['success'] = True
				response['message'] = "Unauthorized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = True
			response['message'] = "Invalid request"
			return response, 400

		time_span_measure = TimeSpanMeasure.find_time_span_measure(id_=time_span_measure_id)

		if time_span_measure:
			data = time_span_measure.to_dict()
			response['success'] = True
			response['data'] = data
			return response, data
		else:
			response['success'] = True
			response['message'] = "Time span measure not found"
			return response, 400

	def view_all(self, role_id):

		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403

		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] = "Unauthorized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Invalid request"
			return response, 400

		data = TimeSpanMeasure.view_all()
		response['success'] = True
		response['data'] = data
		return response, data

	def delete_time_span_measure(self, role_id, time_span_measure_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403

		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] = "Unauthorized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Invalid request"
			return response, 400

		time_span_measure = TimeSpanMeasure.find_time_span_measure(id_=time_span_measure_id)

		if time_span_measure:
			try:
				time_span_measure.delete_time_span_measure()
				response['success'] = True
				response['message'] = "Deleted succesfully."
				return response, 204

			except Exception as e:
				logger.exception(e)
				response['success'] = False
				response['message'] = "Unable to delete time span measure"
				return response, 500
		else:
			response['success'] = False
			response['message'] = "Time span measure not found"
			return response, 404
