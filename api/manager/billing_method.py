from api.models import BillingMethod
from logger import logger


class BillingMethodManager(object):
	def create(self, name, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403

		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] = "Unauthorized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		duplicate_method = BillingMethod.find_billing_method(name=name.title())

		if duplicate_method:
			response['success'] = False
			response['message'] = "Billing method {} already exists.".format(name.title())
			return response, 409

		new_billing_method = BillingMethod.create(billing_method_name=name)
		if new_billing_method:
			response['success'] = True
			response['data'] = new_billing_method
			return response, 201

		response['success'] = False
		response['message'] = 'Error encountered in creating billing method {}'.format(name.title())
		return response, 400

	def edit(self, name, role_id, billing_method_id):

		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403

		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] = "Unauthorized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Invalid request"
			return response, 400

		duplicate_method = BillingMethod.find_billing_method(name=name)
		if duplicate_method:
			response['success'] = False
			response['message'] = "Billing method with name {} already exists".format(name.title())
			return response, 409

		billing_method = BillingMethod.find_billing_method(id_=billing_method_id)

		if billing_method:
			data = billing_method.edit(name=name)
			response['success'] = True
			response['data'] = data
			return response, 201
		else:
			response['success'] = False
			response['message'] = "Billing method not found"
			return response, 404


		response['success'] = False
		response['message'] = "Error editing billing method"
		return response, 500

	def view_one(self, role_id, billing_method_id):

		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403

		# try:
		# 	if int(role_id) < 3:
		# 		response['success'] = True
		# 		response['message'] = "Unauthorized"
		# 		return response, 403
		# except ValueError as e:
		# 	logger.exception(e)
		# 	response['success'] = True
		# 	response['message'] = "Invalid request"
		# 	return response, 400

		billing_method = BillingMethod.find_billing_method(id_=billing_method_id)

		if billing_method:
			data = billing_method.to_dict()
			response['success'] = True
			response['data'] = data
			return response, data
		else:
			response['success'] = True
			response['message'] = "Billing method not found"
			return response, 400

	def view_all(self, role_id):
		print(f"role_id = {role_id}")
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403

		# try:
		# 	if int(role_id) < 5:
		# 		response['success'] = False
		# 		response['message'] = "Unauthorized"
		# 		return response, 403
		# except ValueError as e:
		# 	logger.exception(e)
		# 	response['success'] = False
		# 	response['message'] = "Invalid request"
		# 	return response, 400

		data = BillingMethod.view_all()
		response['success'] = True
		response['data'] = data
		return response, data

	def delete_billing_method(self, role_id, billing_method_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = "Unauthorized"
			return response, 403

		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] = "Unauthorized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Invalid request"
			return response, 400

		billing_method = BillingMethod.find_billing_method(id_=billing_method_id)

		if billing_method:
			try:
				billing_method.delete_billing_method()
				response['success'] = True
				response['message'] = "Deleted succesfully."
				return response, 204

			except Exception as e:
				logger.exception(e)
				response['success'] = False
				response['message'] = "Unable to delete billing method"
				return response, 500
		else:
			response['success'] = False
			response['message'] = "Billing method not found"
			return response, 404
