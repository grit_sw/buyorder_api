from flask import Flask
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy

from config import config
from logger import logger


db = SQLAlchemy()
api = Api(doc='/doc/')


def create_api(config_name):
	app = Flask(__name__)
	try:
		init_config = config[config_name]()
	except KeyError as e:
		logger.exception(e)
		raise
	except Exception as e:
		logger.exception(e)
		# For unforseen exceptions
		raise

	print('Running in {} Mode'.format(init_config))
	config_object = config.get(config_name)

	app.config.from_object(config_object)
	config_object.init_app(app)

	db.init_app(app)

	from api.controllers import buy_api as ns1
	from api.controllers import billing_method_api as ns2
	from api.controllers import time_span_measure_api as ns3
	api.add_namespace(ns1, path='/buy')
	api.add_namespace(ns2, path="/billing-method")
	api.add_namespace(ns3, path="/time-span-measure")

	api.init_app(app)

	return app
