from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates


CreateBillingMethod = namedtuple('CreateBillingMethod', ['name'])


class BillingMethodSchema(Schema):
	name = fields.String(required=True)

	@post_load
	def create_billing_method(self, data):
		return CreateBillingMethod(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError("Billing method name is required.")
