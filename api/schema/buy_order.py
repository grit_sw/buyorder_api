from collections import namedtuple

import arrow
from marshmallow import (Schema, ValidationError, fields, post_load, validates,
                         validates_schema)

from logger import logger

CreateOrder = namedtuple('CreateOrder', [
	'billing_method_id',
	'billing_unit_id',
	'rate',
	'cost_limit',
	'time_span',
	'time_span_measure_id',
	'auto_renew',
	'renew_frequency',
	'start_date_time',
	'end_date_time',
	'user_group_id',
	'facility_id',
	'subnet_id',
	'prosumer_id'
])


class BuyOrderSchema(Schema):
	billing_method_id = fields.String(required=True)
	billing_unit_id = fields.String(required=True)
	rate = fields.Integer(required=True)
	cost_limit = fields.Integer(allow_none=True)
	time_span = fields.Integer(allow_none=True)
	time_span_measure_id = fields.String(allow_none=True)
	auto_renew = fields.Boolean(required=True)
	renew_frequency = fields.String(allow_none=True)
	start_date_time = fields.String(allow_none=True)
	end_date_time = fields.String(allow_none=True)
	user_group_id = fields.String(required=True)
	facility_id = fields.String(required=True)
	subnet_id = fields.String(required=True)
	prosumer_id = fields.String(required=True)

	@post_load
	def create_order(self, data):
		return CreateOrder(**data)

	@validates_schema(pass_original=True)
	def validate_match(self, _, original_data):
		start_date_time = original_data['start_date_time']
		end_date_time = original_data['end_date_time']
		current_date_time = arrow.now("Africa/Lagos")
		order_window = current_date_time.shift(minutes=5)

		try:
			arrow.get(start_date_time)
		except (arrow.parser.ParserError, ValueError):
			raise ValidationError('Invalid start time')

		try:
			arrow.get(end_date_time)
		except (arrow.parser.ParserError, ValueError):
			raise ValidationError('Invalid stop time')

		if start_date_time:
			start_date_time = arrow.get(start_date_time).replace(tzinfo="Africa/Lagos")
			current_date_time = arrow.now("Africa/Lagos")
			order_window = current_date_time.shift(minutes=5)

			if start_date_time < current_date_time:
				raise ValidationError('Start time cannot be in the past.')

			if start_date_time < order_window:
				raise ValidationError('Order made too soon. Please allow a window greater than 5 minutes.')

			if end_date_time:
				end_date_time = arrow.get(end_date_time).replace(tzinfo="Africa/Lagos")
				if start_date_time > end_date_time:
					raise ValidationError('End date must be greater than start date.')

		else:
			if end_date_time:
				raise ValidationError('No end date without start date')

		if original_data['time_span'] or original_data['time_span_measure_id']:
			if not(original_data['time_span'] and original_data['time_span_measure_id']):
				raise ValidationError("Both time span and its measure are required")

	@validates('billing_method_id')
	def validate_billing_method_id(self, value):
		if not value:
			raise ValidationError('Billing method required.')

	@validates('billing_unit_id')
	def validate_billing_unit(self, value):
		if not value:
			raise ValidationError('Billing method required.')

	@validates('rate')
	def validate_rate(self, value):
		try:
			if int(value) <= 0:
				raise ValidationError('Invalid rate.')
		except Exception:
			raise ValidationError('Invalid input.')

	@validates('cost_limit')
	def validate_cost_limit(self, value):
		if value:
			try:
				int(value)
				if value < 0:
					raise ValidationError('Invalid amount.')
			except Exception:
				raise ValidationError('Invalid entry')

	@validates('time_span')
	def validate_time_span(self, value):
		if value:
			try:
				if int(value) <= 0:
					raise ValidationError("Invalid time span.")
			except Exception:
				raise ValidationError("Invalid entry.")

	@validates('time_span_measure_id')
	def validate_time_span_measure(self, value):
		if value:
			if not value:
				raise ValidationError("Time span measure id is required.")

	@validates('auto_renew')
	def validate_auto_renew(self, value):
		if not isinstance(value, bool):
			raise ValidationError('Auto renewal information required.')

	@validates('renew_frequency')
	def validate_renew_frequency(self, value):
		if value:
			if not value:
				raise ValidationError("Renew frequency is required.")

	@validates('start_date_time')
	def validate_start_date_time(self, value):
		if value:
			try:
				arrow.get(value)
			except (arrow.parser.ParserError, ValueError) as e:
				logger.exception(e)
				raise ValidationError('Order start date time required.')

	@validates('end_date_time')
	def validate_end_date_time(self, value):
		if value:
			try:
				arrow.get(value)
			except (arrow.parser.ParserError, ValueError) as e:
				logger.exception(e)
				raise ValidationError('Order start date time required.')

	@validates('user_group_id')
	def validate_user_group_id(self, value):
		if not value:
			raise ValidationError('User group ID required.')

	@validates('facility_id')
	def validate_facility_id(self, value):
		if not value:
			raise ValidationError('Facility ID required.')

	@validates('subnet_id')
	def validate_subnet_id(self, value):
		if not value:
			raise ValidationError("Subnet ID required")

	@validates('prosumer_id')
	def validate_prosumer_id(self, value):
		if not value:
			raise ValidationError("Consumer ID required")
