from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates


CreateTimeSpanMeasure = namedtuple('CreateTimeSpanMeasure', ['name'])


class TimeSpanSchema(Schema):
	name = fields.String(required=True)

	@post_load
	def create_time_span_measure(self, data):
		return CreateTimeSpanMeasure(**data)

	@validates('name')
	def validate_name(self, value):
		if not value:
			raise ValidationError("Time span measure name is required.")
