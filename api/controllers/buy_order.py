from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import OrderManager
from api.schema import BuyOrderSchema
from logger import logger

buy_api = Namespace('buy', description='Api for managing buy orders')

order = buy_api.model('Buy Order', {
	'billing_method_id': fields.String(required=True, description="Billing method id of user"),
	'billing_unit_id': fields.String(required=True, description="Billing unit id of user"),
	'rate': fields.Float(required=True, description='Sell order rate a user selects'),
	'cost_limit' : fields.Integer(required=False, description="Cost limit of user"),
	'time_span': fields.Integer(required=False, desription="Indicates how long the order should last for in total"),
	'time_span_measure_id': fields.String(required=False, description="Indicates the id for the time span limit"),
	'auto_renew': fields.Boolean(required=True, description='Indicates if the user wants automatically renew their buy order'),
	'renew_frequency': fields.String(required=True, description='Shows how often the user wants to renew their buy order'),
	'start_date_time': fields.String(description='Start date and time for daily schedule of buy order'),
	'end_date_time': fields.String(description='End date and time for daily schedule of buy order'),
	'user_group_id': fields.String(required=True, description='User group ID of the user.'),
	'facility_id': fields.String(required=True, description='Facility ID of the user\'s meter.'),
	'subnet_id': fields.String(required=True, description='Subnet ID of the user\'s meter.'),
	'prosumer_id': fields.String(required=True, description='Prosumer ID of the user\'s meter.'),
	'schedule_id': fields.String(required=False, description='Schedule ID of the user\'s meter.'),
})


@buy_api.route('/self-new-order/')
class NewSelfBuyOrder(Resource):
	"""
		Api to create a new energy order
	"""
	@buy_api.expect(order)
	def post(self):
		"""
			Method for a user to create a new buy order for himself.
			@param: payload: data for the update
			@returns: response and status code
		"""
		response = {}
		schema = BuyOrderSchema(strict=True)
		data = request.values.to_dict()
		_payload = api.payload or data
		try:
			payload = schema.load(_payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = e.messages
			return response, 400
		role_id = request.cookies.get('role_id')
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthorised.'
			return response, 403
		group_id = request.cookies.get('group_id', 200)
		try:
			if int(role_id) < 4:
				if not all([role_id, group_id]):
					response['success'] = False
					response['message'] = "Unauthourized"
					return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403
		if not payload.get('user_group_id'):
			# for external services
			payload['user_group_id'] = group_id
		manager = OrderManager()
		resp, code = manager.create_order(payload)
		return resp, code


@buy_api.route('/admin-new-order/')
class NewAdminBuyOrder(Resource):
	"""
		Api to create a new energy order
	"""
	@buy_api.expect(order)
	def post(self):
		"""
			Method for an admin to create a new buy order for a user.
			@param: payload: data for the update
			@returns: response and status code
		"""
		response = {}
		schema = BuyOrderSchema(strict=True)
		data = request.values.to_dict()
		_payload = api.payload or data
		try:
			payload = schema.load(_payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['message'] = e.messages
			return response, 400
		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 3:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403


		admin_id = request.cookies.get('user_id')
		manager = OrderManager()
		resp, code = manager.create_order(payload, admin_id=admin_id)
		return resp, code


@buy_api.route('/all/<string:facility_id>/')
class AllBuyOrders(Resource):
	"""
		Api to manage all buy orders
	"""

	def get(self, facility_id):
		"""
			HTTP method to get all buy orders
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 3:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403


		facilities = request.cookies.get('facilities')
		# facilities = {facility_id : "b8fd6b43-90d7-4bce-ac06-33f5c22e2728", facility_id : "string"}
		if not facility_id in set(facilities):
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)

		resp, code = manager.all_orders_in_facility(facility_id, page, size)
		return resp, code


@buy_api.route('/by-subnet/<string:subnet_id>/')
class SubnetBuyOrders(Resource):
	"""
		Api to manage all buy orders orders of a subnet.
	"""

	def get(self, subnet_id):
		"""
			HTTP method to get all buy orders orders of a subnet.
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 3:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)

		resp, code = manager.all_subnet_orders(subnet_id, page, size)
		return resp, code


@buy_api.route('/one-order/<string:order_id>/')
class OneBuyOrder(Resource):
	"""
		Api to manage individual buy orders
	"""
	def get(self, order_id):
		"""
			HTTP method to get a customer's buy order
			@param: customer_id: ID of the customer
			@returns: response and status code
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 1:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403


		manager = OrderManager()
		resp, code = manager.one_order(order_id)
		return resp, code


@buy_api.route('/my-orders/<string:user_group_id>/')
class MyBuyOrders(Resource):
	"""
		Api to manage individual buy orders
	"""
	def get(self, user_group_id):
		"""
			Route to view all the buy orders a user has.
			@param: user_group_id: group ID of the user
			@returns: response and status code
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 1:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403


		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)
		manager = OrderManager()
		resp, code = manager.my_orders(user_group_id, page, size)
		return resp, code


@buy_api.route('/edit-order/<string:order_id>/')
class EditBuyOrder(Resource):
	"""
		Api to create a edit energy order
	"""
	@buy_api.expect(order)
	def post(self, order_id):
		"""
			Method for a user to create a edit buy order for herself.
			@param: payload: data for the update
			@returns: response and status code
		"""
		response = {}
		schema = BuyOrderSchema(strict=True)
		data = request.values.to_dict()
		_payload = api.payload or data
		try:
			payload = schema.load(_payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = e.messages
			return response, 400
		role_id = request.cookies.get('role_id')
		group_id = request.cookies.get('group_id')
		try:
			if not all([role_id, group_id]) or int(role_id) < 1:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		payload['user_group_id'] = group_id
		manager = OrderManager()
		payload['order_id'] = order_id
		resp, code = manager.edit_order(payload)
		return resp, code


@buy_api.route('/find-sellorder-match/')
class FindBuyOrderMatch(Resource):

	@buy_api.expect(order)
	def post(self):
		"""
			Get all buy orders that match a sell order
			@returns: response and status code
		"""
		response = {}
		schema = BuyOrderSchema(strict=True)
		data = request.values.to_dict()
		_payload = api.payload or data
		try:
			payload = schema.load(_payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = e.messages
			return response, 400

		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 6:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		resp, code = manager.find_order_match(payload)
		return resp, code


@buy_api.route('/delete-order/<string:order_id>/')
class DeleteBuyOrder(Resource):
	def delete(self, order_id):
		"""
			HTTP method to delete a buy order
			@param: order_id: ID of the order
			@returns: response and status code
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 2:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403


		manager = OrderManager()
		resp, code = manager.delete_order(order_id)
		return resp, code


@buy_api.route('/delete-user-group/<string:user_group_id>/')
class DeleteUserGroupBuyOrders(Resource):
	def delete(self, user_group_id):
		"""
			HTTP method to delete all the buy orders in a user group
			@param: user_group_id: ID of the user group
			@returns: response and status code
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 3:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403


		manager = OrderManager()
		resp, code = manager.delete_user_group(user_group_id)
		return resp, code


@buy_api.route('/delete-facility/<string:facility_id>/')
class DeleteFacilityBuyOrders(Resource):
	def delete(self, facility_id):
		"""
			HTTP method to delete all the buy orders in a facility
			@param: facility_id: ID of the user group
			@returns: response and status code
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 5:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403


		manager = OrderManager()
		resp, code = manager.delete_facility(facility_id)
		return resp, code
