from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import  BillingMethodManager
from api.schema import  BillingMethodSchema
from logger import logger


billing_method_api = Namespace('billing_method', description='Api for managing billing methods')

billing_method = billing_method_api.model('BillingMethod', {
	'name': fields.String(required=True, description="Name of billing method")
})

@billing_method_api.route('/create/')
class CreateBillingMethod(Resource):
	"""
		Api to create a billing method
	"""

	@billing_method_api.expect(billing_method)
	def post(self):
		"""
			HTTP method to create a billing method
			@param: payload: data for the update
			@returns: response and status code
		"""

		response = {}
		schema = BillingMethodSchema(strict=True)
		data = request.values.to_dict()
		payload = data or api.payload

		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		role_id = request.cookies.get('role_id')
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthorised.'
			return response, 403

		new_payload['role_id'] = role_id
		manager = BillingMethodManager()
		resp, code = manager.create(**new_payload)
		return resp, code

@billing_method_api.route('/edit/<string:billing_method_id>/')
class EditBillingMethod(Resource):
	"""
	 	Api to edit existing billing method
	"""

	@billing_method_api.expect(billing_method)
	def post(self, billing_method_id):
		"""
			HTTP method to edit a billing method
			@param: payload: data for the update
			@return: response and status code
		"""

		response = {}
		data = request.values.to_dict()
		payload = data or api.payload
		schema = BillingMethodSchema()

		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = BillingMethodManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		new_payload['billing_method_id'] = billing_method_id
		resp, code = manager.edit(**new_payload)
		return resp, code

@billing_method_api.route("/view-billing-method/<string:billing_method_id>/")
class ViewOneBillingMethod(Resource):
	"""
		Api to view a billing method
	"""

	def get(self, billing_method_id):
		"""
			HTTP method to view one billing method
			@returns: response and status code
		"""

		manager = BillingMethodManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_one(role_id, billing_method_id)
		return resp, code

@billing_method_api.route("/view-all/")
class ViewAllBillingMethods(Resource):
	"""
		Api to view all billing methods
	"""
	def get(self):
		"""
			HTTP method to view all billing methods
		"""
		manager = BillingMethodManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_all(role_id)
		return resp, code

@billing_method_api.route("/delete-billing-method/<string:billing_method_id>/")
class DeleteBillingMethod(Resource):
	"""
		Api to delete billing method
	"""

	def delete(self, billing_method_id):
		"""
			HTTP method to delete a billing method
			@returns: response and status code
		"""

		manager = BillingMethodManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_billing_method(role_id, billing_method_id)
		return resp, code
