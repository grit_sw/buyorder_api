from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import TimeSpanManager
from api.schema import TimeSpanSchema
from logger import logger


time_span_measure_api = Namespace('time_span_measure', description='Api for managing time span measures')

time_span_measure = time_span_measure_api.model('TimeSpanMeasure', {
	'name': fields.String(required=True, description="Name of time span measure")
})

@time_span_measure_api.route('/create/')
class CreateTimeSpanMeasure(Resource):
	"""
		Api to create a time span measure
	"""

	@time_span_measure_api.expect(time_span_measure)
	def post(self):
		"""
			HTTP method to create a time span measure
			@param: payload: data for the update
			@returns: response and status code
		"""

		response = {}
		schema = TimeSpanSchema(strict=True)
		data = request.values.to_dict()
		payload = data or api.payload

		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		manager = TimeSpanManager()
		resp, code = manager.create(**new_payload)
		return resp, code

@time_span_measure_api.route('/edit/<string:time_span_measure_id>/')
class EditTimeSpanMeasure(Resource):
	"""
	 	Api to edit existing time span measure
	"""

	@time_span_measure_api.expect(time_span_measure)
	def post(self, time_span_measure_id):
		"""
			HTTP method to edit a time span measure
			@param: payload: data for the update
			@return: response and status code
		"""

		response = {}
		data = request.values.to_dict()
		payload = data or api.payload
		schema = TimeSpanSchema()

		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = TimeSpanManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		new_payload['time_span_measure_id'] = time_span_measure_id
		resp, code = manager.edit(**new_payload)
		return resp, code

@time_span_measure_api.route("/view-time-span-measure/<string:time_span_measure_id>/")
class ViewOneTimeSpanMeasure(Resource):
	"""
		Api to view a time span measure
	"""

	def get(self, time_span_measure_id):
		"""
			HTTP method to view one time span measure
			@returns: response and status code
		"""

		manager = TimeSpanManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_one(role_id, time_span_measure_id)
		return resp, code

@time_span_measure_api.route("/view-all/")
class ViewAllTimeSpanMeasures(Resource):
	"""
		Api to view all time span measures
	"""
	def get(self):
		"""
			HTTP method to view all time span measures
		"""
		manager = TimeSpanManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.view_all(role_id)
		return resp, code

@time_span_measure_api.route("/delete-time-span-measure/<string:time_span_measure_id>/")
class DeleteTimeSpanMeasure(Resource):
	"""
		Api to delete time span measure
	"""

	def delete(self, time_span_measure_id):
		"""
			HTTP method to delete a time span measure
			@returns: response and status code
		"""

		manager = TimeSpanManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_time_span_measure(role_id, time_span_measure_id)
		return resp, code
