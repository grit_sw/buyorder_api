import json
from time import sleep

from avro import schema
from confluent_kafka import Producer
from confluent_kafka.avro import AvroProducer
from confluent_kafka.avro import CachedSchemaRegistryClient as SchemaRegistry
# from requests.exceptions import ConnectionError

from api.exceptions import (KafkaConnectionError, TopicMissingError,
                            ValueSchemaNotFound)
from logger import logger


def from_schema_file(schema_file):
	with open(schema_file, "r+") as avro_file:
		schema_str = avro_file.read()
	return schema.Parse(schema_str)


class KafkaProducer(object):
	"""docstring for KafkaProducer"""

	def __init__(self, bootstrap_servers=None, schema_registry_uri=None, topic_schema_mapping=None):
		super(KafkaProducer, self).__init__()
		self.bootstrap_servers = bootstrap_servers
		self.schema_registry_uri = schema_registry_uri
		self.schema_registry = SchemaRegistry(self.schema_registry_uri)
		self.topic_schema_mapping = topic_schema_mapping
		self.avro_producer = AvroProducer({
			'bootstrap.servers': self.bootstrap_servers,
			'schema.registry.url': self.schema_registry_uri,
		},
			# default_value_schema=self.value_schema
		)
		self.json_producer = Producer({
			'bootstrap.servers': self.bootstrap_servers,
		})
		self.init_schema()

	def init_schema(self):
		for _, schema_info in self.topic_schema_mapping.items():
			schema_subject = schema_info[0]
			schema_file = schema_info[1]
			try:
				self.check_registry(schema_subject, schema_file)
			except KafkaConnectionError as e:
				logger.critical(e)
				continue

	@property
	def value_schema(self, schema_subject, schema_file):
		if schema_subject is None or self.schema_registry is None:
			raise ValueError('Schema subject AND schema_registry required')
		_, schema_, _ = self.schema_registry.get_latest_schema(schema_subject)
		if schema_ is None:
			schema_ = self.check_registry(schema_subject, schema_file)
		return schema_

	def check_registry(self, schema_subject, schema_file):
		try:
			logger.info('Performing Registry Check')
			retry_count = 0
			while True:
				try:
					_, b, _ = self.schema_registry.get_latest_schema(schema_subject)
					break
				except ConnectionError as e:
					logger.critical(e)
					retry_count += 1
					sleep(5)
					if retry_count == 5:
						raise KafkaConnectionError(
							'Cannot connect to Schema Registry at {} after {} tries'.format(
								self.schema_registry_uri,
								retry_count
							)
						)
			if b is None:
				avro_schema = from_schema_file(schema_file)
				schema_id = self.schema_registry.register(
					schema_subject,
					avro_schema
				)
				logger.info('New schema registration at {} with subject {}\n'.format(self.schema_registry.url, schema_subject))
				return self.schema_registry.get_by_id(schema_id)
			else:
				logger.info('Schema already registered at {} with subject {}\n'.format(self.schema_registry.url, schema_subject))
				return b
		except ConnectionError as e:
			logger.critical(e)
			return None

	def send_message(self, topic, message, use_avro=True):
		if not use_avro:
			self.json_producer.produce(topic, json.dumps(message).encode('utf-8'))
			return
		if topic in self.topic_schema_mapping:
			schema_subject, schema_file = self.topic_schema_mapping[topic]
		else:
			logger.info("\n\n\n\n\n\n")
			logger.info("self.topic_schema_mapping - {}".format(self.topic_schema_mapping))
			logger.info("\n\n\n\n\n\n")
			raise TopicMissingError("Topic {} is not found.".format(topic))
		value_schema = self.check_registry(schema_subject, schema_file)
		if not isinstance(message, dict):
			raise ValueError("Message should be formated in dict format not {}".format(type(message)))
		if value_schema is None:
			raise ValueSchemaNotFound("Value schema for topic {} not found at {}".format(topic, self.schema_registry_uri))
		self.avro_producer.produce(
			topic=topic,
			value=message,
			value_schema=value_schema
		)
		self.avro_producer.flush()
		sleep(0.001)
