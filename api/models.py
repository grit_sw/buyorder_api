from uuid import uuid4

import arrow
from flask import current_app
from sqlalchemy import and_, or_
from sqlalchemy_utils import ArrowType

from api import db
from api.kafka_mixin import KafkaMixin
from logger import logger


class Base(db.Model):
	__abstract__ = True
	id_ = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
	date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
								onupdate=db.func.current_timestamp())


class BillingMethod(Base):

	"""
		Billing Method Table
	"""
	id_ = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=True, nullable=False)
	billing_method_id = db.Column(db.String(100), unique=True, nullable=False)
	buy_order = db.relationship('BuyOrder', backref="billing_method", cascade="all, delete, delete-orphan", lazy="dynamic")

	def __init__(self, name):
		self.name = name.title()
		self.billing_method_id = str(uuid4())

	@staticmethod
	def create(billing_method_name):
		new = BillingMethod(name=billing_method_name)

		try:
			db.session.add(new)
			db.session.commit()
			db.session.refresh(new)
		except Exception as e:
			db.session.rollback()
			logger.exception(e)
			return

		return new.to_dict()

	def edit(self, name=name):
		self.name = name.title()
		try:
			db.session.add(self)
			db.session.commit()
			db.session.refresh(self)
		except Exception as e:
			logger.exception(e)
			db.session.rollback()
			return

		return self.to_dict()

	@staticmethod
	def find_billing_method(id_=None, name=None):
		if id_:
			billing_method = BillingMethod.query.filter_by(billing_method_id=id_).first()
			return billing_method
		elif name:
			billing_method = BillingMethod.query.filter_by(name=name.title()).first()
			return billing_method
		else:
			raise Exception("Name or ID required")

	@staticmethod
	def setup():
		methods = ['Pay as You Go', 'Prepaid']
		for method in methods:
			if not BillingMethod.find_billing_method(name=method):
				BillingMethod.create(method)

	@staticmethod
	def view_all():
		all_billing_methods = BillingMethod.query.order_by(BillingMethod.name.asc()).all()
		return [billing_method.to_dict() for billing_method in all_billing_methods]

	def delete_billing_method(self):
		db.session.delete(self)
		db.session.commit()

	def to_dict(self):
		method_dict = {
			'billing_method_name': self.name,
			'billing_method_id': self.billing_method_id
		}
		return method_dict


class TimeSpanMeasure(Base):

	"""
		Time Span Measure Table
	"""
	id_ = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=True, nullable=False)
	time_span_measure_id = db.Column(db.String(100), unique=True, nullable=False)
	buy_order = db.relationship('BuyOrder', backref="time_span_measure", cascade="all, delete, delete-orphan", lazy="dynamic")

	def __init__(self, name):
		self.name = name.title()
		self.time_span_measure_id = str(uuid4())

	@staticmethod
	def create(time_span_measure_name):
		new = TimeSpanMeasure(name=time_span_measure_name)
		success = False

		try:
			db.session.add(new)
			db.session.commit()
			db.session.refresh(new)
		except Exception as e:
			db.session.rollback()
			logger.exception(e)
			return e, success

		return new.to_dict()

	def edit(self, name=name):
		self.name = name.lower()
		try:
			db.session.add(self)
			db.session.commit()
			db.session.refresh(self)
		except Exception as e:
			logger.exception(e)
			db.session.rollback()

		return self.to_dict()

	@staticmethod
	def find_time_span_measure(id_=None, name=None):
		if id_:
			time_span_measure = TimeSpanMeasure.query.filter_by(time_span_measure_id=id_).first()
			return time_span_measure
		elif name:
			time_span_measure = TimeSpanMeasure.query.filter_by(name=name.title()).first()
			return time_span_measure
		else:
			raise Exception("Name or ID required")

	@staticmethod
	def setup():
		measures = ['Days', 'Weeks', 'Months']
		for measure in measures:
			if not TimeSpanMeasure.find_time_span_measure(name=measure):
				TimeSpanMeasure.create(measure)

	@staticmethod
	def view_all():
		all_time_span_measures = TimeSpanMeasure.query.order_by(TimeSpanMeasure.id_.asc()).all()
		return [time_span_measure.to_dict() for time_span_measure in all_time_span_measures]

	def delete_time_span_measure(self):
		db.session.delete(self)
		db.session.commit()

	def to_dict(self):
		method_dict = {
			'time_span_measure_name': self.name,
			'time_span_measure_id': self.time_span_measure_id
		}
		return method_dict


class Scheduler(db.Model):
	id_ = db.Column(db.Integer, autoincrement=True, primary_key=True)
	order_id = db.Column(db.String(50), db.ForeignKey("buy_order.order_id"))
	schedule_id = db.Column(db.String(50), nullable=False)
	user_group_id = db.Column(db.String(50), nullable=False)
	start_time = db.Column(db.String(10), nullable=False)
	end_time = db.Column(db.String(10), nullable=False)
	date_of_month = db.Column(db.String(10), nullable=True)
	day_of_week = db.Column(db.String(10), nullable=True)
	termination_set = db.Column(db.Boolean, nullable=False)
	termination_date = db.Column(ArrowType(timezone=True), nullable=True)

	def __init__(self, order, user_group_id, start_time, end_time, date_of_month, day_of_week,
				termination_set, termination_date):
		self.user_group_id = user_group_id
		self.start_time = start_time
		self.end_time = end_time
		self.date_of_month = date_of_month
		self.day_of_week = day_of_week
		self.termination_set = termination_set
		self.termination_date = termination_date
		self.buy_order = order
		self.schedule_id = str(uuid4())

	@staticmethod
	def create_schedule(order):
		termination_set = all([order.time_span, order.time_span_measure_id])
		if termination_set:
			time_span_measure = TimeSpanMeasure.find_time_span_measure(id_=order.time_span_measure_id)

			if  time_span_measure.name == "months":
				termination_date = arrow.get(order.start_date_time).shift(months=order.time_span)
			elif time_span_measure.name == "weeks":
				termination_date = arrow.get(order.start_date_time).shift(weeks=order.time_span)
			elif time_span_measure.name == "days":
				termination_date = arrow.get(order.start_date_time).shift(days=order.time_span)
			else:
				termination_date = None
		else:
			termination_date = None

		schedule = Scheduler(
			order=order,
			user_group_id=order.user_group_id,
			start_time=arrow.get(order.start_date_time).format('HH-mm-ss').replace("-", ":"),
			end_time=arrow.get(order.end_date_time).format('HH-mm-ss').replace("-", ":"),
			date_of_month=arrow.get(order.start_date_time).format('Do'),
			day_of_week=arrow.get(order.start_date_time).format('dddd'),
			termination_date=termination_date,
			termination_set=bool(termination_date)
		)
		return schedule

	@staticmethod
	def edit_schedule(order, data):
		termination_set = all([data['time_span'], data['time_span_measure_id']])

		if termination_set:
			time_span_measure = TimeSpanMeasure.find_time_span_measure(id_=data['time_span_measure_id'])
			print("Time Span Measure", time_span_measure.time_span_measure_id)
			if  time_span_measure.name == "months":
				termination_date = arrow.get(order.start_date_time).shift(months=order.time_span)
			elif time_span_measure.name == "weeks":
				termination_date = arrow.get(order.start_date_time).shift(weeks=order.time_span)
			elif time_span_measure.name == "days":
				termination_date = arrow.get(order.start_date_time).shift(days=order.time_span)
			else:
				termination_date = None
		else:
			termination_date = None

		schedule = order.schedule.first()

		schedule.user_group_id = data['user_group_id']
		schedule.start_time = arrow.get(data['start_date_time']).format('HH-mm-ss').replace("-", ":")
		schedule.end_time = arrow.get(data['end_date_time']).format('HH-mm-ss').replace("-", ":")
		schedule.date_of_month = arrow.get(data['start_date_time']).format('Do')
		schedule.day_of_week = arrow.get(data['start_date_time']).format('dddd')
		schedule.termination_date = termination_date
		schedule.termination_set = bool(termination_date)
		db.session.add(schedule)

		return schedule.to_dict()

	# @staticmethod
	# def delete_schedule(order):
	# 	db.session.delete(order.schedule.first())
	# 	db.session.commit()

	def to_dict(self):
		dict_schedule = {
			'schedule_id': self.schedule_id,
			'user_group_id': self.user_group_id,
			'start_time': self.start_time,
			'end_time': self.end_time,
			'date_of_month': self.date_of_month,
			'day_of_week': self.day_of_week,
			'termination_set': self.termination_set,
			'termination_date': self.termination_date
		}
		return dict_schedule


class BuyOrder(KafkaMixin, Base):
	# __tablename__ = "buy_orders"
	id_ = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	billing_method_id = db.Column(db.String(70), db.ForeignKey('billing_method.billing_method_id'))
	billing_unit_id = db.Column(db.String(70), nullable=False)
	rate = db.Column(db.Integer, nullable=True)
	cost_limit = db.Column(db.Integer, nullable=True)
	time_span = db.Column(db.Integer, nullable=True)
	time_span_measure_id = db.Column(db.String(70), db.ForeignKey('time_span_measure.time_span_measure_id'))
	auto_renew = db.Column(db.Boolean, default=False)
	renew_frequency = db.Column(db.String(50), nullable=True)
	start_date_time = db.Column(ArrowType(timezone=True), nullable=True)
	end_date_time = db.Column(ArrowType(timezone=True), nullable=True)
	user_group_id = db.Column(db.String(70), nullable=False)
	created_by = db.Column(db.String(70), nullable=False)
	order_id = db.Column(db.String(70), nullable=False, unique=True)
	facility_id = db.Column(db.String(70), nullable=False)
	subnet_id = db.Column(db.String(70), nullable=False) # required because a facility AND a prosumer 
	# could be part of multiple subnets in the future.
	prosumer_id = db.Column(db.String(70), nullable=False)
	schedule = db.relationship('Scheduler', backref="buy_order", lazy='dynamic')

	def __init__(self, billing_method_id, billing_unit_id, rate, cost_limit, time_span, time_span_measure_id, auto_renew, renew_frequency,
	start_date_time, end_date_time, user_group_id, created_by, facility_id, subnet_id, prosumer_id):
		self.billing_method_id = billing_method_id
		self.billing_unit_id = billing_unit_id
		self.rate = rate
		self.cost_limit = cost_limit
		self.time_span = time_span
		self.time_span_measure_id = time_span_measure_id
		self.auto_renew = auto_renew
		self.renew_frequency = renew_frequency
		self.start_date_time = start_date_time
		self.end_date_time = end_date_time
		self.user_group_id = user_group_id
		self.created_by = created_by
		self.facility_id = facility_id
		self.subnet_id = subnet_id
		self.prosumer_id = prosumer_id
		self.order_id = str(uuid4())

	@staticmethod
	def create_order(billing_method_id, billing_unit_id, rate, cost_limit, time_span, time_span_measure_id, auto_renew, renew_frequency,
						start_date_time, end_date_time, user_group_id, created_by, facility_id, subnet_id, prosumer_id):
		"""Method that saves the created plan in the database"""
		success = False
		order = BuyOrder(
			billing_method_id=billing_method_id,
			billing_unit_id=billing_unit_id,
			rate=rate,
			cost_limit=cost_limit,
			time_span=time_span,
			time_span_measure_id=time_span_measure_id,
			auto_renew=auto_renew,
			renew_frequency=renew_frequency,
			start_date_time=arrow.get(start_date_time).replace(tzinfo="Africa/Lagos"),
			end_date_time=arrow.get(end_date_time).replace(tzinfo="Africa/Lagos"),
			user_group_id=user_group_id,
			created_by=created_by,
			facility_id=facility_id,
			subnet_id=subnet_id,
			prosumer_id=prosumer_id
		)

		db.session.add(order)
		Scheduler.create_schedule(order)
		db.session.add(order)
		try:
			db.session.commit()
			db.session.refresh(order)

		except Exception as e:
			logger.exception(e)
			success = False
			db.session.rollback()
			return e, success
		success = True
		return order.to_dict(), success

	def edit_order(self, data):
		"""
			Method to edit an existing buy order
			@param: data: Details of what will be changed
		"""
		self.billing_method_id = data['billing_method_id']
		self.billing_unit_id = data['billing_unit_id']
		self.rate = float(data['rate'])
		self.cost_limit = data['cost_limit']
		self.time_span = data['time_span']
		self.time_span_measure_id = data['time_span_measure_id']
		self.prosumer_id = data['prosumer_id']
		if self.auto_renew != data['auto_renew']:
			self.auto_renew = not self.auto_renew
		# self.auto_renew = data['auto_renew']
		self.renew_frequency = data['renew_frequency']
		self.start_date_time = arrow.get(data['start_date_time']).replace(tzinfo="Africa/Lagos")
		self.end_date_time = arrow.get(data['end_date_time']).replace(tzinfo="Africa/Lagos")
		Scheduler.edit_schedule(self, data)

		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def find_order_match(max_price, start_date, end_date, facility_id):
		# max_price here indicates the price that the sell order will be billed at.
		"""
			Method to obtain a buy order matching a sell order.
		"""
		matches = BuyOrder.query.filter_by(facility_id=facility_id).filter(BuyOrder.max_price >= max_price
		).filter(BuyOrder.start_date >= start_date).filter(BuyOrder.end_date <= end_date).all()
		return matches

	@staticmethod
	def delete_order(order):
		"""Delete one order."""
		# Scheduler.delete_schedule(order)
		db.session.delete(order)
		db.session.commit()
		return

	@staticmethod
	def delete_all_orders(order_obj):
		"""Delete multiple orders."""
		for order in order_obj.all():
			db.session.delete(order)
		db.session.commit()
		return

	@staticmethod
	def get_order(order_id=None, user_group_id=None, facility_id=None, subnet_id=None):
		if order_id:
			order = BuyOrder.query.filter_by(order_id=order_id).first()
			return order

		if user_group_id:
			order = BuyOrder.query.filter_by(user_group_id=user_group_id)
			return order

		if facility_id:
			order = BuyOrder.query.filter_by(facility_id=facility_id)
			return order

		if subnet_id:
			order = BuyOrder.query.filter_by(subnet_id=subnet_id)
			return order

	@staticmethod
	def get_user_order(user_group_id):
		order = BuyOrder.get_order(user_group_id=user_group_id)
		if not order.first():
			return None
		return order

	@staticmethod
	def get_facility_order(facility_id):
		order = BuyOrder.get_order(facility_id=facility_id)
		if not order.first():
			return None
		return order

	@staticmethod
	def get_subnet_order(subnet_id):
		order = BuyOrder.get_order(subnet_id=subnet_id)
		if not order.first():
			return None
		return order

	@staticmethod
	def one_order(order_id):
		order = BuyOrder.get_order(order_id)
		if not order:
			return order
		return order.to_dict()

	@staticmethod
	def all_facility_orders(facility_id):
		orders = BuyOrder.query.filter_by(facility_id=facility_id)
		return orders

	@staticmethod
	def facility_orders(facility_id, page_no, size):
		orders = BuyOrder.all_facility_orders(facility_id)
		orders = orders.order_by(BuyOrder.date_modified.desc()).paginate(page_no, size, False)
		return orders

	@staticmethod
	def all_subnet_orders(subnet_id):
		orders = BuyOrder.query.filter_by(subnet_id=subnet_id)
		return orders

	@staticmethod
	def subnet_orders(subnet_id, page_no, size):
		orders = BuyOrder.all_subnet_orders(subnet_id)
		orders = orders.order_by(BuyOrder.date_modified.desc()).paginate(page_no, size, False)
		return orders

	@staticmethod
	def all_my_orders(user_group_id):
		orders = BuyOrder.query.filter_by(user_group_id=user_group_id)
		return orders

	@staticmethod
	def my_orders(user_group_id, page_no, size):
		orders = BuyOrder.all_my_orders(user_group_id)
		orders = orders.order_by(BuyOrder.date_modified.desc()).paginate(page_no, size, False)
		return orders

	@staticmethod
	def clashing_times(user_group_id, facility_id, start_date_time, end_date_time):
		"""
			This method checks for time clashes between orders.
			It prevents orders from being created that overright the run time of an existing order.
			This strictly checks the time fields of orders.
			If a change is to be made, an EDIT to the order is advised.
			This query makes use of several conditions to check an order.
			It is not immediately obvious how to tell the user what condition they are violating.
			The inspiration for this query is found in the stackoverflow answer here: https://stackoverflow.com/a/46161522/4909255
		"""
		start_date_time_between_existing_order = and_(BuyOrder.start_date_time <= start_date_time, BuyOrder.end_date_time > start_date_time).self_group()
		end_date_time_between_existing_order = and_(BuyOrder.start_date_time < end_date_time, BuyOrder.end_date_time >= end_date_time).self_group()
		order_within_existing_order = and_(BuyOrder.start_date_time <= start_date_time, BuyOrder.end_date_time >= end_date_time).self_group()
		order_enveloping_existing_order = and_(BuyOrder.start_date_time > start_date_time, BuyOrder.end_date_time < end_date_time).self_group()

		clash_conditions = [start_date_time_between_existing_order, end_date_time_between_existing_order, order_within_existing_order, order_enveloping_existing_order]

		# Clash occurs when one of the clash conditions is met
		or_clauses = or_(*clash_conditions)
		time_clash = BuyOrder.query.filter_by(user_group_id=user_group_id).filter_by(facility_id=facility_id).filter(or_clauses).all()

		return time_clash

	def payload_format(self):
		dict_order = {
			'billing_method_id' : self.billing_method_id,
			'billing_unit_id' : self.billing_unit_id,
			'rate' : self.rate,
			'cost_limit' : self.cost_limit,
			'time_span' : self.time_span,
			'time_span_measure_id' : self.time_span_measure_id,
			'auto_renew' : self.auto_renew,
			'renew_frequency' : self.renew_frequency,
			'start_date_time': str(self.start_date_time),
			'end_date_time': str(self.end_date_time),
			'user_group_id': self.user_group_id,
			'facility_id' : self.facility_id,
			'subnet_id': self.subnet_id
		}
		return dict_order

	def kafka_dict(self):
		dict_order = {
			'kafka_topic': current_app.config.get('NEW_BUYORDER_TOPIC'),
			'user_group_id': self.user_group_id,
			'subnet_id' : self.subnet_id,
			'prosumer_id' : self.prosumer_id,
			'order_id': self.order_id,

			'start_date_time': str(self.start_date_time),
			'end_date_time': str(self.end_date_time),

			'rate' : self.rate,

			'cost_limit' : self.cost_limit,
			'time_span' : self.time_span,
			'time_span_measure_id' : self.time_span_measure_id,

			'auto_renew' : self.auto_renew,
			'renew_frequency' : self.renew_frequency,

			'billing_unit_id' : self.billing_unit_id,
			'billing_method_id' : self.billing_method_id,
		}
		return dict_order

	def to_dict(self):
		date_created = str(arrow.get(self.date_created))
		dict_order = {
			'order_id': self.order_id,
			'billing_method_id' : self.billing_method_id,
			'billing_unit_id' : self.billing_unit_id,
			'rate' : self.rate,
			'cost_limit' : self.cost_limit,
			'time_span' : self.time_span,
			'time_span_measure_id' : self.time_span_measure_id,
			'auto_renew' : self.auto_renew,
			'renew_frequency' : self.renew_frequency,
			'start_date_time': str(self.start_date_time),
			'end_date_time': str(self.end_date_time),
			'user_group_id': self.user_group_id,
			'facility_id' : self.facility_id,
			'subnet_id' : self.subnet_id,
			'date_created': date_created,
			'schedule': [sched.to_dict() for sched in self.schedule.all()]
		}
		return dict_order


db.event.listen(db.session, 'before_commit', KafkaMixin.before_commit)
db.event.listen(db.session, 'after_commit', KafkaMixin.after_commit)
