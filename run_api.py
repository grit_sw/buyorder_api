# Test Server.
from os import environ

from arrow import now
from flask import request
from flask_migrate import Migrate

from api import create_api, db
from logger import logger

if environ.get('FLASK_ENV') is None:
	print('FLASK_ENV not set')
	exit()
mode = environ.get('FLASK_ENV')
app = create_api(mode)
migrate = Migrate(app, db)
to_migrate = app.config.get('MIGRATE_DB')

with app.app_context():
	# db.reflect()
	# db.drop_all()
	db.create_all()

@app.before_first_request
def setup_billing_methods():
	from api.models import BillingMethod, TimeSpanMeasure
	BillingMethod.setup()
	TimeSpanMeasure.setup()
	logger.info(BillingMethod.view_all())
	logger.info(TimeSpanMeasure.view_all())


@app.after_request
def log_info(response):
	try:
		log_data = {
			'connection': request.headers.get('Connection'),
			'ip_address': request.remote_addr,
			'browser_name': request.user_agent.browser,
			'user_device': request.user_agent.platform,
			'referrer': request.referrer,
			'request_url': request.url,
			'host_url': request.host_url,
			'status_code': response.status_code,
			'date': str(now('Africa/Lagos')),
			'location': response.location,
		}
		logger.info('buyorder_api_logs : {}'.format(log_data))
	except Exception as e:
		logger.exception("buyorder_api_logs: {}".format(e))
	return response


# if to_migrate == 'Yes':
#     with app.app_context():
#         print('Upgrading database')
#         upgrade()
